# Déploiement de wsdataselect avec Helm

Dans ce dépôt, on trouvera:

- wsdataselect : la helm chart pour le déploiement
- helm-values: les valeurs appliquées dans les différents contextes
  - staging
  - production
  
Il y a des valeurs chiffrées dans des fichier `secrets.yaml`.

# commande de déploiement

Ce déploiement nécessite au préalable, l'installation avec Helm des volumes de données.

``` sh
context=$(kubectl config view --minify -o jsonpath='{..namespace}') && helm secrets upgrade -i ${context}-wsdataselect wsdataselect --values  https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/salt/helmvalues/global-mounts.yaml --values helm-values/${context}/values.yaml --values helm-values/${context}/secrets.yaml
```

